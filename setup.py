from setuptools import setup, find_packages
from codecs import open
from os import path
from barista import __VERSION__ as version

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='barista',
    version=version,
    description='',
    long_description=long_description,
    url='https://bitbucket.org/prgr4m/barista',
    author='John Boisselle',
    author_email='prgr4m@yahoo.com',
    license='MIT',
    # See - https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'Topic :: Utilities',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: JavaScript'
    ],
    keywords='nodejs nodeenv',
    packages=find_packages(exclude=['tests', 'docs']),
    install_requires=['nodeenv', 'argcomplete'],
    extras_require={
        'dev': ['sphinx', 'livereload']
    },
    entry_points={
        'console_scripts': ['barista=barista.main:main']
    }
)
