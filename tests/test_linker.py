# -*- coding: utf-8 -*-
import unittest
import os
from os import path
import shutil
from barista import BARISTA_ENVS
from barista import linker
import tempfile

TEST_PACKAGE_JSON = path.join(path.abspath(path.dirname(__file__)),
                              'test_data', 'package.json')

# THIS TEST HAS THE FOLLOWING REQUIREMENTS AND NEED TO BE PERFORMED IN THE
# FOLLOWING ORDER TO RUN THEM:
#
# 1. create a test nodejs env by running: barista mk test_nvenv
# 2. activate the nodejs env by sourcing the activation script
# 3. activate the virtualenv that this package is under for development for
#    testing
# 4. run this script
#
# The problem is that subshells need to reproduce their parent environment.


class TestBaristaLinker(unittest.TestCase):
    def setUp(self):
        self.env_name = 'test_nvenv'
        self.env_path = path.join(BARISTA_ENVS, self.env_name)
        self.tmp_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmp_dir)

    def test_parse_package_json(self):
        # test package.json is not present within the current directory
        self.assertRaises(IOError, linker.parse_package_json, pkg='')
        expected_from_test_sample = [
            ('lodash', '^4.1.0'), ('coffee-script', '^1.10.0')
        ]
        dependencies = linker.parse_package_json(pkg=TEST_PACKAGE_JSON)
        self.assertEqual(expected_from_test_sample, dependencies)

    def test_get_node_modules_path(self):
        """THIS TEST WILL FAIL IF NOT RUN UNDER AN ACTIVATED NODEJS VENV"""
        test_env_node_modules = path.join(self.env_path, 'lib', 'node_modules')
        prefix_call_node_modules = linker.get_node_modules_path()
        self.assertEqual(test_env_node_modules, prefix_call_node_modules)

    def test_setup_links_creates_proper_symlinks_to_global(self):
        """THIS TEST WILL FAIL IF NOT RUN UNDER AN ACTIVATED NODEJS VENV"""
        cwd = os.getcwd()
        target_pkg_file = path.join(self.tmp_dir, 'package.json')
        shutil.copyfile(TEST_PACKAGE_JSON, target_pkg_file)
        os.chdir(self.tmp_dir)
        test_modules = ('lodash', 'coffee-script')
        global_modules_path = path.join(self.env_path, 'lib',
                                        'node_modules')
        linker.setup_links(linker.parse_package_json())
        local_modules_path = path.join(self.tmp_dir, 'node_modules')
        local_modules = [path.join(local_modules_path, m)
                         for m in os.listdir(local_modules_path)
                         if m not in ('.bin', '.', '..')]
        global_modules = [path.join(global_modules_path, x)
                          for x in test_modules]
        for m in local_modules:
            self.assertTrue(path.exists(m))
            self.assertIn(path.realpath(m), global_modules)
        os.chdir(cwd)

if __name__ == "__main__":
    unittest.main()
