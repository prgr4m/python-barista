# -*- coding: utf-8 -*-
import unittest
from os import path
import shutil
from barista import (
    BARISTA_HOME, BARISTA_ENVS, BARISTA_PKGS, utils
)


class TestBaristaUtils(unittest.TestCase):
    def test_home_check_creates_data_dir_if_it_doesnt_exist(self):
        if path.exists(BARISTA_HOME):
            shutil.rmtree(BARISTA_HOME)
        utils.init_check()
        self.assertTrue(path.exists(BARISTA_HOME))
        self.assertTrue(path.exists(BARISTA_ENVS))
        self.assertTrue(path.exists(BARISTA_PKG))

if __name__ == "__main__":
    unittest.main()
