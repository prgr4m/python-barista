# -*- coding: utf-8 -*-
import unittest
import os
from os import path
import shutil
from barista import BARISTA_ENVS, main


class TestBaristaMain(unittest.TestCase):
    def setUp(self):
        self.env_name = 'test_nvenv'
        self.nodevenv = path.join(BARISTA_ENVS, self.env_name)

    def tearDown(self):
        if path.exists(self.nodevenv):
            shutil.rmtree(self.nodevenv)

    def create_fake_venv(self):
        if not path.exists(self.nodevenv):  # just to be sure ;)
            os.mkdir(self.nodevenv)

    def test_delete_node_venv_actually_deletes_venv_in_data_directory(self):
        self.create_fake_venv()
        main.delete_node_venv(self.env_name)
        self.assertNotIn(self.env_name, os.listdir(BARISTA_ENVS))

    def test_create_node_venv_creates_new_venv_in_data_directory(self):
        # makes a call to nodeenv and is dependent upon internet
        # should run in a try/catch... um try/except block
        main.create_node_venv(self.env_name)
        self.assertIn(self.env_name, os.listdir(BARISTA_ENVS))

if __name__ == "__main__":
    unittest.main()
