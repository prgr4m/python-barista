Usage
-----

To get help just run ``barista -h`` on the shell and you should get this 
display: ::

    $ barista -h
    usage: barista [-h]
                   {ls:env,make:env,rm:env,activate,linker,info} ...

    A nodeenv manager for prototypal nodejs virtual environments

    venv management:
        ls:env              list available nodejs virtual environments
        make:env            make a nodejs virtual environment
        rm:env              remove a nodejs virtual environment
        activate            generate activation code

    package management:
        linker              npm linker utility

    positional arguments:
      {ls:env,make:env,rm:env,activate,linker,info}
        info                display version and miscellaneous information

    optional arguments:
      -h, --help            show this help message and exit

If you ever need help with any of the commands just run 
``barista [command] -h`` for further details. I've tried to be as thorough as 
possible.


Examples
--------

Managing Nodejs Venvs
^^^^^^^^^^^^^^^^^^^^^

Listing nodejs venvs
~~~~~~~~~~~~~~~~~~~~
::

    $ barista ls:env
    available nodejs venv(s):
            pixijs-playground
            phaserjs-playground
            vuejs-sandbox

Make a nodejs venv
~~~~~~~~~~~~~~~~~~
::

    $ barista make:env phaserjs-playground
    create nodejs venv: phaserjs-playground
    calling nodeenv...
     * Install node (5.5.0)... done.
    to activate run: barista activate phaserjs-playground

By default this always calls nodeenv to fetch the latest versions of nodejs and 
npm in prebuilt form. If you wish, you can call ``nodeenv -h`` to find out how 
to get a specific version and build from source.

You can also have the activation code generated/pasted to the clipboard by 
passing a ``-a`` or ``--activate`` option along with the command. Also, if you 
choose to just use a system-wide nodejs on your system, just pass ``-s`` or 
``--system`` to barista and it'll tell nodeenv to use that instead.

*activation code pasted to clipboard:*
::

    $ barista make:env -a phaserjs-playground

*using system node + npm:*
::

    $ barista make:env -s phaserjs-playground

*or both:*
::

    $ barista make:env -a -s phaserjs-playground

Remove a nodejs venv
~~~~~~~~~~~~~~~~~~~~
::

    $ barista rm:env angular-sandbox
    angular-sandbox - has been removed


Activation code
~~~~~~~~~~~~~~~

For those of you unfamiliar with how python's virtualenv works, you have to 
activate an environment by calling a shell script that creates a subshell,  
changes pathing for isolation and adds functions specific to your environment 
so the tooling can do its thing. Barista uses nodeenv to do that part but 
keeps all of your nodejs venvs in one place (similar to virtualenvwrapper) but 
without the shell voodoo.

::

    $ barista activate phaserjs-development
    Activation code copied to the clipboard; press: <Shift+F10><P><Enter>
    To deactivate run: deactivate_node

Just paste the code from your clipboard: ``<Shift+F10>``, press ``<P>``, hit 
``<Enter>`` and your nodejs venv is activated. You can also type: ::

    $ xclip -selection clipboard -o

to get the same effect, but I prefer to just hit 4 keys rather than typing all 
of that out. If you don't have ``xclip`` installed, barista will barf and tell 
you to install it with a debian-based example.


NPM Linker
~~~~~~~~~~

This is the primary reason as to why I created this tool. Since nodeenv creates 
isolated nodejs environments (not just per version but completely isolated) the 
linker installs the required packages into the global isolated node_modules. 

There are two requirements for the linker to work:

1. You need to have an activated nodejs venv to install the packages to
2. a package.json file in the same directory

::

    (vuejs-sandbox)$ barista linker

