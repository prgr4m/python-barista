Requirements and Installation
-----------------------------

* `Python`_ 2.x or 3.x
* A unix-like operating system (built on a Linux Mint 17.3 machine)
* xclip to be installed (debian-based ex: $ sudo apt-get install xclip)

.. _Python: http://python.org

To install a sdist run: ::

    $ pip install https://bitbucket.org/prgr4m/barista/downloads/barista-0.5.0.tar.gz

Of course there is a ``barista-0.5.0-py2.py3-none-any.whl`` as well.

When installed via pip, it should also install nodeenv which does most of the 
grunt work like fetching different versions of nodejs and the shell hacks for 
a virtualenv. This tool is like virtualenvwrapper but pure python and has a 
different interface than lsvirtualenv and friends.

When the program is first run with a command, the program creates a data 
directory to store the nodejs venvs (virtual environments). You can control 
where it get installed at by setting an environment variable:

.. code-block:: bash

   export BARISTA_HOME=/path/to/where/you/want/barista

By default barista creates its data directory at: $HOME/.barista

