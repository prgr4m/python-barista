Introduction
------------

Barista is a mini cli application for managing virtual environments for nodejs 
using `nodeenv`_. So why the name? A barista in this context, manages and 
creates custom 'java'(script) environments mainly for 
**prototypal development** while trying to cut down on npm modules littering 
your hard drive.

.. _nodeenv: https://github.com/ekalinin/nodeenv

If you are drinking the javascript kool-aid like everyone else is (and there 
are legitimate reasons to), whenever you are working on a project, you only 
have the option of installing packages locally or globally. Yes, there are 
tools like `nvm`_ which allow you to run multiple versions of nodejs but not 
environments per project type and you have to install half the internet each 
time to get a prototype of something up and running. On average per local 
install (at least with basic tooling from my experience -- you know the usual 
karma + protractor + phantomjs + pals -- when mocha + jsdom would be able to 
do the job) is ~150MB+.

.. image:: _static/barista-diagram.svg
   :width: 100%

That adds up over time especially since the output is usually much smaller 
when sent to the browser.

.. _nvm: https://github.com/creationix/nvm

Features
--------

* list, create, remove nodejs virtual environments
* simple package.json archive management for nodejs virtual environments
* npm linker for per project type installations
* generates nodejs virtual environment activation code to paste into terminal 
  from clipboard
* command-line autocompletion (bash)
