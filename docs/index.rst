.. barista documentation master file, created by
   sphinx-quickstart on Mon Feb  1 18:51:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Barista's documentation
=======================

.. include:: introduction.rst.inc
.. include:: installation.rst.inc
.. include:: usage.rst.inc
.. include:: changelog.rst
.. include:: license.rst
