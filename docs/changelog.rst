Changelog
---------

Version 0.5.0
^^^^^^^^^^^^^
**05-FEB-2016**

* rewrote the parser to be not so cryptic for user
* activate option added after initial creation of a nodejs venv
* option to use system nodejs when creating a nodejs venv
* command-line autocompletion (tested only on bash)
* removed package.json archiving capability

Version 0.4.0
^^^^^^^^^^^^^
**01-FEB-2016**

* package.json archive management
* user guide

Version 0.3.0
^^^^^^^^^^^^^
**31-JAN-2016**

* shortened cli rm:delete, ls:list, mk:create/make, ln:linker
* integrated linker for project-typed environments
* added clipboard capability for pasting venv sourcing code (linux only)
* added package.json archive capability when run with linker

Version 0.2.0
^^^^^^^^^^^^^
**30-JAN-2016**

* created simple cli to app
* mapped cli to callable routes
* list, create and delete nodejs venvs
* moved venvs into their own subdirectory

Version 0.1.0
^^^^^^^^^^^^^
**29-JAN-2016**

* initial creation of project
* creation of data directory if not present
