#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os
from os import path
import shutil
from subprocess import call, Popen, PIPE
from . import BARISTA_ENVS
from .cli import parser
from .utils import init_check, display_version_info
from .linker import linker_init


def list_node_venv(silent=False):
    nodejs_venvs = os.listdir(BARISTA_ENVS)
    if silent:
        return nodejs_venvs
    if any(nodejs_venvs):
        print("available nodejs venv(s):")
        for venv in nodejs_venvs:
            print("\t{}".format(venv))
    else:
        print("no available nodejs venv(s).")
        print("run: `barista mk -h` for more info.")


def create_node_venv(nodeenv_name, activate_env=False, use_system=False):
    nenv_dir = path.join(BARISTA_ENVS, nodeenv_name)
    if path.exists(nenv_dir):
        print("{} - already exists!".format(nodeenv_name))
    else:
        print("create nodejs venv: {}".format(nodeenv_name))
        print("calling nodeenv...")
        nodeenv_call = ['nodeenv', '--quiet']
        if use_system:
            nodeenv_call.extend(['-n', 'system', nenv_dir])
        else:
            nodeenv_call.extend(['--prebuilt', nenv_dir])
        call(nodeenv_call)
        msg = "to activate run: barista activate {}".format(nodeenv_name)
        print(msg)
        if activate_env:
            generate_activation_code(nodeenv_name)


def delete_node_venv(nodeenv_name):
    delete_target = path.join(BARISTA_ENVS, nodeenv_name)
    if path.exists(delete_target):
        shutil.rmtree(delete_target)
        msg = "{} - has been removed".format(nodeenv_name)
        print(msg)
    else:
        print("{} - can't be removed because it doesn't exist"
              .format(nodeenv_name))


def clone_node_venv(nodeenv_name, new_nvenv_name, use_symlinks=False):
    # target regex/replace in bin: activate, shim
    pass


def generate_activation_code(nodeenv_name):
    known_venvs = list_node_venv(silent=True)
    if nodeenv_name not in known_venvs:
        print("{} is not a known nodejs venv!".format(nodeenv_name))
        print("run: 'barista ls' to show known environments")
    else:
        activate_path = path.join(BARISTA_ENVS, nodeenv_name, 'bin',
                                  'activate')
        activate_cmd = "source {}".format(activate_path)
        try:
            p = Popen(['xclip', '-selection', 'clipboard'], stdin=PIPE)
            p.communicate(activate_cmd.encode('utf-8'))
            instructions = "Activation code copied to the clipboard; {}"
            key_sequence = "press: <Shift+F10><P><Enter>"
            print(instructions.format(key_sequence))
            print("To deactivate run: deactivate_node")
        except OSError:
            print("xclip isn't installed! run: sudo apt-get install xclip")


def main():
    args = parser.parse_args()
    init_check()
    call_map = {
        'ls:env': list_node_venv,
        'make:env': create_node_venv,
        'rm:env': delete_node_venv,
        'activate': generate_activation_code,
        'linker': linker_init,
        'info': display_version_info
    }
    action = call_map[args.subparser_name]
    named_args = ('make:env', 'rm:env', 'activate')
    if args.subparser_name in named_args:
        if args.subparser_name == 'make:env':
            action(args.name, args.activate, args.system)
        else:
            action(args.name)
    else:
        action()

if __name__ == "__main__":
    main()
