# -*- coding: utf-8 -*-
from __future__ import print_function
import os
from os import path
from . import BARISTA_HOME, BARISTA_ENVS, BARISTA_PKGS, __VERSION__ as version


def init_check():
    barista_subdirs = [BARISTA_ENVS, BARISTA_PKGS]
    if not path.exists(BARISTA_HOME):  # brand spankin' new
        for d in barista_subdirs:
            os.makedirs(d)
    else:  # create the expected directory if it doesn't exist
        for d in barista_subdirs:
            if not path.exists(d):
                os.makedirs(d)


def display_version_info():
    info_display = """barista: version {version}

Paths:
    BARISTA_HOME: {barista_home}
    BARISTA_ENVS: {barista_envs}

Autocompletion:
    If you are having issues with autocompletion then you can perform
    the following:

    {completion_msg}
       -or-
    activate-global-python-argcomplete --dest=- > file

    [Note]: The file's contents should then be sourced in ~/.bashrc if not
    activated globally
"""
    complete_msg_1 = "sudo activate-global-python-argcomplete"
    complete_mgs_2 = "--dest=/etc/bash_completion.d/barista"
    completion_msg = "{} {}".format(complete_msg_1, complete_mgs_2)
    info_data = {
        'version': version,
        'barista_home': BARISTA_HOME,
        'barista_envs': BARISTA_ENVS,
        'barista_pkgs': BARISTA_PKGS,
        'completion_msg': completion_msg
    }
    print(info_display.format(**info_data))


def nodejs_env_name_completer(prefix, **kwargs):
    return (n for n in os.listdir(BARISTA_ENVS) if n.startswith(prefix))
