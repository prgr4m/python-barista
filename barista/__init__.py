# -*- coding: utf-8 -*-
from os import environ, path
from sys import platform

__VERSION__ = '0.5.0'

user = '~user' if platform in ('win32', 'cygwin') else '~'
BARISTA_HOME = environ.get('BARISTA_HOME', path.join(path.expanduser(user),
                                                     '.barista'))
BARISTA_ENVS = path.join(BARISTA_HOME, 'venvs')
BARISTA_PKGS = path.join(BARISTA_HOME, 'packages')

# When I implement installing a certain version...?
# RELEASES_SRC = "https://nodejs.org/dist/index.json"

__all__ = ['BARISTA_HOME', 'BARISTA_ENVS', 'BARISTA_PKGS']
