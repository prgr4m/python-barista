# -*- coding: utf-8 -*-
# PYTHON_ARGCOMPLETE_OK
import argparse
from argparse import RawDescriptionHelpFormatter
import argcomplete
from .utils import nodejs_env_name_completer

# Written this way to have grouped subparsers
barista_desc = """
A nodeenv manager for prototypal nodejs virtual environments

venv management:
    ls:env              list available nodejs virtual environments
    make:env            make a nodejs virtual environment
    rm:env              remove a nodejs virtual environment
    activate            generate activation code

package management:
    linker              npm linker utility
"""

parser = argparse.ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                                 description=barista_desc)
subparsers = parser.add_subparsers(dest='subparser_name')
# subparsers = parser.add_subparsers(dest='subparser_name',
#                                    help='sub-command help')

ls_env_desc = "List available nodejs virtual environments"
ls_env_parser = subparsers.add_parser('ls:env', description=ls_env_desc)

mk_env_desc = "Make a nodejs virtualenv (latest nodejs + npm)"
mk_env_parser = subparsers.add_parser('make:env', description=mk_env_desc)
mk_env_parser.add_argument('name', help="Name of nodejs venv to make")
mk_env_activate_help = "generate activation code after making new venv"
mk_env_parser.add_argument('-a', '--activate', action='store_true',
                           default=False, help=mk_env_activate_help)
mk_env_system_help = "Use system nodejs + npm instead"
mk_env_parser.add_argument('-s', '--system', action='store_true',
                           default=False, help=mk_env_system_help)

rm_env_desc = "Remove a nodejs virtualenv managed by barista"
rm_env_parser = subparsers.add_parser('rm:env', description=rm_env_desc)
rm_env_parser.add_argument('name', help="Name of nodejs venv to delete")\
    .completer = nodejs_env_name_completer

activate_desc = "Generate activation code to a known nodejs venv"
activate_parser = subparsers.add_parser('activate', description=activate_desc)
activate_parser.add_argument('name', help="Name of nodejs venv to activate")\
    .completer = nodejs_env_name_completer

linker_desc = "NPM linker utility for conserving disk space when prototyping"
linker_parser = subparsers.add_parser('linker', description=linker_desc)

# adding info
info_help = "display version and miscellaneous information"
info_parser = subparsers.add_parser('info', description=info_help,
                                    help=info_help)

argcomplete.autocomplete(parser)
