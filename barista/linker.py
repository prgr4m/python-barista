from __future__ import print_function
import json
import os
from os import path
from subprocess import check_output, call, CalledProcessError
import sys

PKG_FILE = 'package.json'


def setup_links(project_deps):
    global_path = get_node_modules_path()
    global_listing = os.listdir(global_path)
    local_pkgs = set([pkg[0] for pkg in project_deps])
    missing_pkgs_set = local_pkgs.difference(global_listing)
    if len(missing_pkgs_set) > 0:
        global_install_cmd = ['npm', 'install', '-g']
        global_install_cmd.extend(["{}@{}".format(p_name, p_ver[1:])
                                   for p_name, p_ver in project_deps
                                   if p_name in missing_pkgs_set])
        call(global_install_cmd)
    for pkg in project_deps:
        call(['npm', 'link', pkg[0]])


def get_node_modules_path():
    prefix_cmd = ['npm', 'config', 'get', 'prefix']
    try:
        prefix = str(check_output(prefix_cmd)).strip()
    except (CalledProcessError, OSError) as e:
        print("you must activate a nodejs venv to have npm on path")
        raise e
    return path.join(prefix, 'lib', 'node_modules')


def parse_package_json(pkg=PKG_FILE):
    try:
        pkg_contents = json.loads(open(pkg, 'r').read())
    except IOError as e:
        err_msg = "package.json file not found in current directory!"
        print(err_msg)
        raise e
    dependencies = []
    if 'dependencies' in pkg_contents:
        dependencies.extend(pkg_contents['dependencies'].items())
    if 'devDependencies' in pkg_contents:
        dependencies.extend(pkg_contents['devDependencies'].items())
    return dependencies


def linker_init():
    try:
        setup_links(parse_package_json())
    except Exception:
        sys.exit()
