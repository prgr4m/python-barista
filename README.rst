=======
Barista
=======

Introduction
============

Barista is a mini cli application for managing virtual environments for nodejs 
using `nodeenv`_. So why the name? A barista in this context manages and 
creates custom 'java'(script) 'drinks' (virtualenvs) mainly for 
*prototypal development* while trying to cut down on npm modules littering your 
hard drive.

I never intended to release this project to the public but if you stumble upon 
it, I hope you find it useful since we're all drinking that javascript kool-aid.

.. _nodeenv: https://github.com/ekalinin/nodeenv


Install
=======
::

    $ [sudo] pip install https://bitbucket.org/prgr4m/barista/downloads/barista-0.5.0.tar.gz

Documentation can be found `here`_.

.. _here: https://bitbucket.org/prgr4m/barista/downloads/barista-0.5.0-docs.zip

**NOTE**:

    When trying to run tests please keep in mind that it is documented within 
    the tests as to why they might fail due to subshell spawning and
    environments not being activated.

License
=======

Copyright (c) 2016 John Boisselle <prgr4m@yahoo.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
