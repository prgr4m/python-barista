#!/usr/bin/env python
# -*- coding: utf-8 -*-
from livereload import Server, shell

server = Server()
includes = ('docs/*.rst', 'docs/*.inc')
for w in includes:
    server.watch(w, shell('make html', cwd='docs'))
server.serve(root='docs/_build/html')
